module.exports = (sequelize, Sequelize) => {
    const Pengetahuan = sequelize.define("pengetahuan", {
      id_faktor: {
        type: Sequelize.STRING
      },
      id_penyakit: {
        type: Sequelize.STRING
      }
    });
  
    return Pengetahuan;
  };