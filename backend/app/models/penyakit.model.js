module.exports = (sequelize, Sequelize) => {
    const Penyakit = sequelize.define("penyakit", {
      nama_penyakit: {
        type: Sequelize.STRING
      },
      detail_penyakit: {
        type: Sequelize.STRING
      },
      solusi_penyakit: {
        type: Sequelize.STRING
      }
    });
  
    return Penyakit;
  };