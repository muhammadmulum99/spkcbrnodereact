import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import Axios from "axios";
import Modal from "react-modal";

// components

// import TableDropdown from "components/Dropdowns/TableDropdown.js";
// const handleSave = () => {
//   // Logika untuk menyimpan perubahan
//   // Anda dapat mengakses nilaiFaktor dan bobotFaktor di sini
//   console.log("Nilai Faktor:", nilaiFaktor);
//   console.log("Bobot Faktor:", bobotFaktor);
//   // Selanjutnya, Anda dapat mengirim perubahan ke server atau tempat penyimpanan lainnya
//   // Setelah itu, tutup modal jika perlu
//   closeEditModal();
// };

export default function CardTable({ color }) {
  const [nilaiFaktor, setNilaiFaktor] = useState(""); // Gantilah nilai awal sesuai kebutuhan Anda
  const [bobotFaktor, setBobotFaktor] = useState(""); // Ganti
  const [faktorPenyebabData, setFaktorPenyebabData] = useState([]);
  const [isEditModalOpen, setEditModalOpen] = useState(false);

  const openEditModal = () => {
    setEditModalOpen(true);
  };

  const closeEditModal = () => {
    setEditModalOpen(false);
  };

  useEffect(() => {
    // Mengambil data faktor penyebab dan bobot faktor dari server
    Axios.get("http://localhost:8080/api/faktor")
      .then((response) => {
        setFaktorPenyebabData(response.data); // Menyimpan data dalam state
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, []);

  const handleDelete = (id) => {
    Axios.delete(`/api/faktor-penyebab/${id}`)
      .then((response) => {
        // Refresh data setelah menghapus
        setFaktorPenyebabData(
          faktorPenyebabData.filter((faktor) => faktor.id !== id)
        );
      })
      .catch((error) => {
        console.error("Error deleting data:", error);
      });
  };
  const handleSave = () => {
    // Logika untuk menyimpan perubahan
    // Anda dapat mengakses nilaiFaktor dan bobotFaktor di sini
    console.log("Nilai Faktor:", nilaiFaktor);
    console.log("Bobot Faktor:", bobotFaktor);
    // Selanjutnya, Anda dapat mengirim perubahan ke server atau tempat penyimpanan lainnya
    // Setelah itu, tutup modal jika perlu
    closeEditModal();
  };
  // ...

  return (
    <>
    <div
    className={
      "relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded " +
      (color === "light" ? "bg-white" : "bg-lightBlue-900 text-white")
    }
  >
    {/* ... */}
    <div className="block w-full overflow-x-auto">
      <table className="items-center w-full bg-transparent border-collapse">
        <thead>
          <tr>
            <th
              className={
                "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                (color === "light"
                  ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                  : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
              }
            >
              Faktor Penyebab
            </th>
            <th
              className={
                "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                (color === "light"
                  ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                  : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
              }
            >
              Bobot Faktor
            </th>
            <th
              className={
                "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                (color === "light"
                  ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                  : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
              }
            >
              Aksi
            </th>
          </tr>
        </thead>
        <tbody>
          {faktorPenyebabData.map((faktorPenyebab) => (
            <tr key={faktorPenyebab.id}>
              <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                {faktorPenyebab.faktor_penyebab}
              </td>
              <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                {faktorPenyebab.bobot_faktor}
              </td>
              <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">


              <button
                onClick={() => openEditModal()}
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mr-2"
              >
                Edit
              </button>


                <button
                  onClick={() => handleDelete(faktorPenyebab.id)}
                  className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded mr-2"
                >
                  Hapus
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  </div>


  // ...
<Modal
  isOpen={isEditModalOpen}
  onRequestClose={closeEditModal}
  contentLabel="Edit Faktor Penyebab"
  style={{
    overlay: {
      backgroundColor: 'rgba(0, 0, 0, 0.75)',
    },
    content: {
      width: '50%',
      maxHeight: '70%',
      margin: 'auto',
      padding: '20px',
      borderRadius: '8px',
    },
  }}
>
  <h2>Edit Faktor Penyebab</h2>
  <form>
    <div className="mb-4">
      <label htmlFor="namaFaktor">Nama Faktor Penyebab</label>
      <input
        type="text"
        id="namaFaktor"
        name="namaFaktor"
        // Gantilah nilai value dengan nilai yang ingin Anda edit
        value={nilaiFaktor} 
        onChange={(e) => setNilaiFaktor(e.target.value)}
        className="form-input mt-1 block w-full"
      />
    </div>
    <div className="mb-4">
      <label htmlFor="bobotFaktor">Bobot Faktor Penyebab</label>
      <input
        type="number"
        id="bobotFaktor"
        name="bobotFaktor"
        // Gantilah nilai value dengan nilai yang ingin Anda edit
        value={bobotFaktor}
        onChange={(e) => setBobotFaktor(e.target.value)}
        className="form-input mt-1 block w-full"
      />
    </div>
    <button type="button" onClick={() => closeEditModal()} className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded mr-2">
      Batal
    </button>
    <button type="submit" onClick={handleSave} className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">
      Simpan
    </button>
  </form>
</Modal>
// ...

</>
  );
}

CardTable.defaultProps = {
  color: "light",
};

CardTable.propTypes = {
  color: PropTypes.oneOf(["light", "dark"]),
};