module.exports = (app) => {
  const { verifySignUp } = require("../middleware");
  const controller = require("../controllers/auth.controller.js");

  var router = require("express").Router();
  // Middleware untuk menangani CORS (Cross-Origin Resource Sharing)
  router.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  // Endpoint untuk registrasi pengguna
  router.post(
    "/api/auth/signup",
    [
      verifySignUp.checkDuplicateUsernameOrEmail, // Middleware untuk memeriksa duplikasi username atau email
      verifySignUp.checkRolesExisted, // Middleware untuk memeriksa peran pengguna
    ],
    controller.signup // Fungsi controller untuk menangani permintaan pendaftaran
  );

  // Endpoint untuk proses login pengguna
  router.post("/api/auth/signin", controller.signin); // Fungsi controller untuk menangani permintaan login
};
