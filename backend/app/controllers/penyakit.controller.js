const db = require("../models");
const Penyakit = db.penyakit;
const Op = db.Sequelize.Op;

// Create and Save a new Penyakit
exports.create = (req, res) => {
    // Validate request
    if (!req.body.namapenyakit || !req.body.detailpenyakit || !req.body.solusipenyakit) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
  
    const foto= "foto.png"
    // Create a Penyakit
    const Penyakits = {
      nama_penyakit: req.body.namapenyakit,
      detail_penyakit: req.body.detailpenyakit,
      solusi_penyakit: req.body.solusipenyakit,
   
    };
  
    // Save Penyakit in the database
    Penyakit.create(Penyakits)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Penyakit."
        });
      });
  };

// Retrieve all Penyakits from the database.
exports.findAll = (req, res) => {
    const nama_penyakit = req.query.nama_penyakit;
    var condition = nama_penyakit ? { nama_penyakit: { [Op.like]: `%${nama_penyakit}%` } } : null;
  
    Penyakit.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Penyakits."
        });
      });
  };

// Find a single Penyakit with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Penyakit.findByPk(id)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).send({
            message: `Cannot find Penyakit with id=${id}.`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving Penyakit with id=" + id
        });
      });
  };

// Update a Penyakit by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
  
    Penyakit.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Penyakit was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Penyakit with id=${id}. Maybe Penyakit was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Penyakit with id=" + id
        });
      });
  };

// Delete a Penyakit with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
  
    Penyakit.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Penyakit was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Penyakit with id=${id}. Maybe Penyakit was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Penyakit with id=" + id
        });
      });
  };

// Delete all Penyakits from the database.
exports.deleteAll = (req, res) => {
    Penyakit.destroy({
      where: {},
      truncate: false
    })
      .then(nums => {
        res.send({ message: `${nums} Penyakits were deleted successfully!` });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all Penyakits."
        });
      });
  };

// Find all published Penyakits
exports.findAllPublished = (req, res) => {
    Penyakit.findAll({ where: { published: true } })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Penyakits."
        });
      });
  };