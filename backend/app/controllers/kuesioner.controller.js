const db = require("../models");
const Kuesioner = db.kuesioner;
const Op = db.Sequelize.Op;

// Create and Save a new Kuesioner
exports.create = (req, res) => {
    // Validate request
    if (!req.body.id_result_cbr || !req.body.id_user || !req.body.id_pengetahuan|| !req.body.nilai_cf|| !req.body.nilai_user) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
  
    const foto= "foto.png"
    // Create a Kuesioner
    const Kuesioners = {
      id_result_cbr: req.body.id_result_cbr,
      id_user: req.body.id_user,
      id_pengetahuan: req.body.id_pengetahuan,
      nilai_cf: req.body.nilai_cf,
      nilai_user: req.body.nilai_user
   
    };
  
    // Save Kuesioner in the database
    Kuesioner.create(Kuesioners)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Kuesioner."
        });
      });
  };

// Retrieve all Kuesioners from the database.
exports.findAll = (req, res) => {
    const nama_Kuesioner = req.query.nama_Kuesioner;
    var condition = nama_Kuesioner ? { nama_Kuesioner: { [Op.like]: `%${nama_Kuesioner}%` } } : null;
  
    Kuesioner.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Kuesioners."
        });
      });
  };

// Find a single Kuesioner with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Kuesioner.findByPk(id)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).send({
            message: `Cannot find Kuesioner with id=${id}.`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving Kuesioner with id=" + id
        });
      });
  };

// Update a Kuesioner by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
  
    Kuesioner.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Kuesioner was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Kuesioner with id=${id}. Maybe Kuesioner was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Kuesioner with id=" + id
        });
      });
  };

// Delete a Kuesioner with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
  
    Kuesioner.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Kuesioner was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Kuesioner with id=${id}. Maybe Kuesioner was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Kuesioner with id=" + id
        });
      });
  };

// Delete all Kuesioners from the database.
exports.deleteAll = (req, res) => {
    Kuesioner.destroy({
      where: {},
      truncate: false
    })
      .then(nums => {
        res.send({ message: `${nums} Kuesioners were deleted successfully!` });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all Kuesioners."
        });
      });
  };

// Find all published Kuesioners
exports.findAllPublished = (req, res) => {
    Kuesioner.findAll({ where: { published: true } })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Kuesioners."
        });
      });
  };