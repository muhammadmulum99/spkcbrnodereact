import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useLocation } from 'react-router-dom';

const InputFormPerhitungan = () => {
    const [dataDariBackend, setDataDariBackend] = useState([]);
    const [selectedOptions, setSelectedOptions] = useState([]);
    const [factorWeights, setFactorWeights] = useState({});
    //const [formData, setFormData] = useState({});
    const location = useLocation();
const initialFormData = location.state?.formData || {}; // Inisialisasi dengan objek kosong jika tidak ada data

const [formData, setFormData] = useState(initialFormData);

    useEffect(() => {
        axios.get('http://localhost:8080/api/faktor')
            .then(response => {
                setDataDariBackend(response.data);
                console.log(response.data);
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }, []);

    const handleDropdownChange = (e) => {
        const selectedFactorId = e.target.value;
        const selectedFactor = dataDariBackend.find((item) => item.id === parseInt(selectedFactorId));

        if (selectedFactor) {
            setSelectedOptions([...selectedOptions, selectedFactor]);
        }
    };

    const handleWeightChange = (factorId, weightValue) => {
        setFactorWeights({ ...factorWeights, [factorId]: weightValue });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('Gejala yang dipilih:', selectedOptions);
        console.log('Bobot faktor:', factorWeights);

        // Lakukan pengiriman data ke server atau jalankan logika diagnosis di sini
        const cases = {
            idfaktor: selectedOptions.map(option => option.id),
            nilaibobot: factorWeights
        };

        const datacases = {
            datacases: [
                {
                  idfaktor: [1,2],
                  nilaibobot: {
                    "1": 0.3,
                    "2": 0.2
                  }
                }
              ]
        }
          

        const dataSendBackend = {
            // datadiri: formData,
            datacases: cases
        };

        axios.post('http://localhost:8080/api/konsultasi/hitungcases', datacases)
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.error(error);
            });
    };

    return (
        <>
            <h1>Data dari Formulir</h1>
            {formData.nama && <p>Nama: {formData.nama}</p>}
            <p>Jenis Kelamin: {formData.jenisKelamin}</p>
            <p>Umur: {formData.umur}</p>
            <p>Nomor HP: {formData.noHp}</p>
            <p>Alamat: {formData.alamat}</p>

            <h2>Data dari Backend</h2>

            <h1>Sistem Pendukung Keputusan Diagnosa Penyakit Pernapasan</h1>

            <form onSubmit={handleSubmit}>
                <div>
                    <label>Pilih Penyakit:</label>
                    <select
                        value={''}
                        onChange={handleDropdownChange}
                    >
                        <option value="">Pilih data</option>
                        {dataDariBackend.map((item) => (
                            <option key={item.id} value={item.id}>
                                {item.faktor_penyebab}
                            </option>
                        ))}
                    </select>
                </div>
                <div>
                    <h3>Data yang dipilih:</h3>
                    <ul>
                        {selectedOptions.map((option, index) => (
                            <li key={index}>
                                {option.faktor_penyebab}:
                                <select
                                    value={factorWeights[option.id] || ''}
                                    onChange={(e) => handleWeightChange(option.id, e.target.value)}
                                >
                                    <option value="">Pilih bobot</option>
                                    <option value="0">Tidak</option>
                                    <option value="0.2">Tidak Tahu</option>
                                    <option value="0.4">Sedikit Yakin</option>
                                    <option value="0.6">Cukup Yakin</option>
                                    <option value="0.8">Yakin</option>
                                    <option value="1">Sangat Yakin</option>
                                </select>
                            </li>
                        ))}
                    </ul>
                </div>
                <button className="bg-indigo-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mr-2" type="submit">Diagnosa</button>
            </form>
        </>
    );
}

export default InputFormPerhitungan;
