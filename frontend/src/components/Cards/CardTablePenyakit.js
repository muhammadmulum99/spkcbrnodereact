import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import Axios from "axios";
import Modal from "react-modal";

Modal.setAppElement("#root");

export default function CardTablePenyakit({ color }) {
  const [namaPenyakit, setNamaPenyakit] = useState("");
  const [detailPenyakit, setDetilPenyakit] = useState("");
  const [solusiPenyakit, setSolusiPenyakit] = useState("");
  const [formData, setFormData] = useState({
    namapenyakit: "",
    detailpenyakit: "",
    solusipenyakit: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (editingItem) {
      // Editing an existing item
      Axios.put(`http://localhost:8080/api/penyakit/${editingItem.id}`, formData)
        .then(function (response) {
          console.log(response);
          closeEditModal();
          // Refresh data or update the state with the edited item
        })
        .catch(function (error) {
          console.error(error);
        });
    } else {
      // Adding a new item
      Axios.post("http://localhost:8080/api/penyakit", formData)
        .then(function (response) {
          console.log(response);
          closeAddModal();
          // Refresh data or update the state with the newly added item
        })
        .catch(function (error) {
          console.error(error);
        });
    }

    // Clear form data
    setFormData({
      namapenyakit: "",
      detailpenyakit: "",
      solusipenyakit: "",
    });
  };

  const [nilaiFaktor, setNilaiFaktor] = useState("");
  const [bobotFaktor, setBobotFaktor] = useState("");
  const [faktorPenyakitData, setfaktorPenyakitData] = useState([]);
  const [isEditModalOpen, setEditModalOpen] = useState(false);
  const [isAddModalOpen, setAddModalOpen] = useState(false);
  const [editingItem, setEditingItem] = useState(null);

  const openAddModal = () => {
    setEditingItem(null); // Clear editing item
    setAddModalOpen(true);
  };

  const closeAddModal = () => {
    setAddModalOpen(false);
  };

  const openEditModal = (item) => {
    setEditingItem(item);
    setEditModalOpen(true);
    // Populate the form fields with the data of the item being edited
    setFormData({
      namapenyakit: item.namapenyakit,
      detailpenyakit: item.detailpenyakit,
      solusipenyakit: item.solusipenyakit,
    });
  };

  const closeEditModal = () => {
    setEditModalOpen(false);
    // Clear form data
    setFormData({
      namapenyakit: "",
      detailpenyakit: "",
      solusipenyakit: "",
    });
  };

  useEffect(() => {
    Axios.get("http://localhost:8080/api/penyakit")
      .then((response) => {
        setfaktorPenyakitData(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, []);

  const handleDelete = (id) => {
    Axios.delete(`http://localhost:8080/api/penyakit/${id}`)
      .then((response) => {
        // Refresh data after deleting
        setfaktorPenyakitData(
          faktorPenyakitData.filter((faktor) => faktor.id !== id)
        );
      })
      .catch((error) => {
        console.error("Error deleting data:", error);
      });
  };

  const handleSave = () => {
    if (editingItem) {
      // Editing an existing item
      Axios.put(`http://localhost:8080/api/penyakit/${editingItem.id}`, formData)
        .then(function (response) {
          console.log(response);
          closeEditModal();
          // Refresh data or update the state with the edited item
        })
        .catch(function (error) {
          console.error(error);
        });
    } else {
      // Adding a new item
      Axios.post("http://localhost:8080/api/penyakit", formData)
        .then(function (response) {
          console.log(response);
          closeAddModal();
          // Refresh data or update the state with the newly added item
        })
        .catch(function (error) {
          console.error(error);
        });
    }

    // Clear form data
    setFormData({
      namapenyakit: "",
      detailpenyakit: "",
      solusipenyakit: "",
    });
  };

  return (
    <>

<div
        className={
          "relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded " +
          (color === "light" ? "bg-white" : "bg-lightBlue-900 text-white")
        }
      >
        {/* ... */}
        <div className="block w-full overflow-x-auto">
          <button
            onClick={() => openAddModal()}
            className="mt-5 ml-5 bg-indigo-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mr-2"
          >
            Add
          </button>
      {/* ... */}
      <table className="items-center w-full bg-transparent border-collapse">
      <thead>
              <tr>
                <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "light"
                      ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
                  Nama Penyakit
                </th>
                <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "light"
                      ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
                  Detail Penyakit
                </th>

                <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "light"
                      ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
                  Solusi Penyakit
                </th>

                <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "light"
                      ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
                  Aksi
                </th>
              </tr>
            </thead>
        <tbody>
          {faktorPenyakitData.map((faktorpenyakit) => (
            <tr key={faktorpenyakit.id}>
              {/* ... */}
              <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                    {faktorpenyakit.nama_penyakit}
                  </td>
                  <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                    {faktorpenyakit.detail_penyakit}
                  </td>
                  <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                    {faktorpenyakit.solusi_penyakit}
                  </td>
              <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                <button
                  onClick={() => openEditModal(faktorpenyakit)} // Step 2
                  className="bg-indigo-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mr-2"
                >
                  Edit
                </button>
                <button
                  onClick={() => handleDelete(faktorpenyakit.id)}
                  className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded mr-2"
                >
                  Hapus
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      </div>
      </div>
      {/* ... */}
      {/* Add Modal */}
      <Modal
        isOpen={isAddModalOpen}
        onRequestClose={closeAddModal}
        contentLabel="Tambah Penyakit"
        style={{
          overlay: {
            backgroundColor: "rgba(0, 0, 0, 0.75)",
          },
          content: {
            width: "50%",
            maxHeight: "70%",
            margin: "auto",
            padding: "20px",
            borderRadius: "8px",
          },
        }}
      >
        <h2>Tambah Penyakit</h2>
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label htmlFor="namapenyakit">Nama Penyakit</label>
            <input
              type="text"
              id="namapenyakit"
              name="namapenyakit"
              value={formData.namapenyakit}
              onChange={handleChange}
              className="form-input mt-1 block w-full"
            />
          </div>
          <div className="mb-4">
            <label htmlFor="detailpenyakit">Detail Penyakit</label>
            <input
              type="text"
              id="detailpenyakit"
              name="detailpenyakit"
              value={formData.detailpenyakit}
              onChange={handleChange}
              className="form-input mt-1 block w-full"
            />
          </div>
          <div className="mb-4">
            <label htmlFor="solusipenyakit">Solusi Penyakit</label>
            <input
              type="text"
              id="solusipenyakit"
              name="solusipenyakit"
              value={formData.solusipenyakit}
              onChange={handleChange}
              className="form-input mt-1 block w-full"
            />
          </div>
          <button
            type="button"
            onClick={closeAddModal}
            className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded mr-2"
          >
            Batal
          </button>
          <button
            type="submit"
            onClick={handleSave}
            className="bg-indigo-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded"
          >
            Simpan
          </button>
        </form>
      </Modal>

      {/* Edit Modal */}
      <Modal
        isOpen={isEditModalOpen}
        onRequestClose={closeEditModal}
        contentLabel="Edit Penyakit"
        style={{
          overlay: {
            backgroundColor: "rgba(0, 0, 0, 0.75)",
          },
          content: {
            width: "50%",
            maxHeight: "70%",
            margin: "auto",
            padding: "20px",
            borderRadius: "8px",
          },
        }}
      >
        <h2>Edit Penyakit</h2>
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label htmlFor="namapenyakit">Nama Penyakit</label>
            <input
              type="text"
              id="namapenyakit"
              name="namapenyakit"
              value={formData.namapenyakit}
              onChange={handleChange}
              className="form-input mt-1 block w-full"
            />
          </div>
          <div className="mb-4">
            <label htmlFor="detailpenyakit">Detail Penyakit</label>
            <input
              type="text"
              id="detailpenyakit"
              name="detailpenyakit"
              value={formData.detailpenyakit}
              onChange={handleChange}
              className="form-input mt-1 block w-full"
            />
          </div>
          <div className="mb-4">
            <label htmlFor="solusipenyakit">Solusi Penyakit</label>
            <input
              type="text"
              id="solusipenyakit"
              name="solusipenyakit"
              value={formData.solusipenyakit}
              onChange={handleChange}
              className="form-input mt-1 block w-full"
            />
          </div>
          <button
            type="button"
            onClick={closeEditModal}
            className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded mr-2"
          >
            Batal
          </button>
          <button
            type="submit"
            onClick={handleSave}
            className="bg-indigo-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded"
          >
            Simpan
          </button>
        </form>
      </Modal>
    </>
  );
}

CardTablePenyakit.defaultProps = {
  color: "light",
};

CardTablePenyakit.propTypes = {
  color: PropTypes.oneOf(["light", "dark"]),
};
