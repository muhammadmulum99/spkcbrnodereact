const db = require("../models");
const PengetahuanModel = db.pengetahuan;
const Penyakit = db.penyakit;
const Op = db.Sequelize.Op;

// Bobot Penyakit (sesuaikan dengan data Anda)
const bobotPenyakit = {
  1: 0.2,  // Bobot untuk penyakit dengan id 1
  2: 0.4,  // Bobot untuk penyakit dengan id 2
  3: 0.6   // Bobot untuk penyakit dengan id 3
};

// Fungsi untuk menghitung kepastian berdasarkan bobot faktor
function hitungKepastian(faktorWeight) {
  const totalBobot = faktorWeight.reduce((acc, weight) => acc + weight, 0);
  return totalBobot / faktorWeight.length;
}

exports.pengetahuan = async (req, res) => {
  try {
    const pengetahuanData = await PengetahuanModel.findAll();
    const pengetahuan = pengetahuanData.map((data) => ({
      faktor: data.id_faktor,
      penyakit: data.id_penyakit,
    }));

    res.json({ pengetahuan });
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({
      message: "Terjadi kesalahan saat mengambil data dari database."
    });
  }
};

exports.perhitungan = async (req, res) => {
  try {
    const { datacases } = req.body;

    if (!datacases || !Array.isArray(datacases) || datacases.length === 0) {
      return res.status(400).json({
        message: "Data diagnosa tidak valid atau kosong."
      });
    }

    let hasilDiagnosa = null;
    let kepastianDiagnosa = -Infinity;

    const pengetahuanData = await PengetahuanModel.findAll();
    const pengetahuan = pengetahuanData.map((data) => ({
      penyakit: data.id_penyakit,
      faktor: data.id_faktor
    }));

    datacases.forEach((datacase) => {
      const { idfaktor, nilaibobot } = datacase;

      if (!Array.isArray(idfaktor) || !nilaibobot || typeof nilaibobot !== 'object') {
        return; // Skip data yang tidak valid
      }

      let kepastianDataCase = 0;
      let penyakitDiagnosa = null;

      pengetahuan.forEach((data) => {
        const { faktor } = data;

        if (Array.isArray(faktor) && idfaktor.every((id) => faktor.includes(id))) {
          const faktorWeight = idfaktor.map((id) => parseFloat(nilaibobot[id]));
          const kepastian = hitungKepastian(faktorWeight) * bobotPenyakit[data.penyakit];

          if (kepastian > kepastianDataCase) {
            kepastianDataCase = kepastian;
            penyakitDiagnosa = data.penyakit;
          }
        }
      });

      if (kepastianDataCase > kepastianDiagnosa) {
        kepastianDiagnosa = kepastianDataCase;
        hasilDiagnosa = { penyakit: penyakitDiagnosa, kepastian: kepastianDataCase };
      }
    });

    if (hasilDiagnosa && kepastianDiagnosa > -Infinity) {
      // Dapatkan penyakit berdasarkan ID dari hasil diagnosa
      const penyakitId = hasilDiagnosa.penyakit;
      const penyakit = await Penyakit.findByPk(1);

      if (penyakit) {
        hasilDiagnosa.nama_penyakit = penyakit.nama_penyakit; // Asumsi model Penyakit memiliki atribut "nama"
        res.json(hasilDiagnosa);
      } else {
        res.status(400).json({ pesan: 'Penyakit tidak ditemukan.' });
      }
    } else {
      res.status(400).json({ pesan: 'Tidak dapat melakukan diagnosa.' });
    }
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({
      message: "Terjadi kesalahan saat memproses permintaan."
    });
  }
};

