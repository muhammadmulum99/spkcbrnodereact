import React, { useState } from "react";
import { useHistory } from "react-router-dom";


const FormInputKonsul = () => {
  const history = useHistory();
  const [formData, setFormData] = useState({
    nama: "",
    jenisKelamin: "",
    umur: "",
    noHp: "",
    alamat: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Lakukan sesuatu dengan data yang dikirimkan, misalnya kirim ke server atau tampilkan di console
    console.log(formData);
    if (formData) {
      const formDataJSON = JSON.stringify(formData);
history.push("/konsultasitahapdua", { formData});
      // Navigasi ke halaman baru dengan mengirimkan data formulir sebagai prop
      //history.push("/konsultasitahapdua", { formData });
    }
  };

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Form Input Data</h1>
      <form onSubmit={handleSubmit} className="grid grid-cols-3 gap-4">
        <div className="col-span-3 md:col-span-1">
          <label htmlFor="nama" className="block text-gray-700">
            Nama:
          </label>
          <input
            type="text"
            id="nama"
            name="nama"
            value={formData.nama}
            onChange={handleChange}
            className="w-full px-4 py-2 border rounded-md focus:outline-none focus:border-blue-500"
            required
          />
        </div>
        <div className="col-span-3 md:col-span-1">
          <label htmlFor="jenisKelamin" className="block text-gray-700">
            Jenis Kelamin:
          </label>
          <select
            id="jenisKelamin"
            name="jenisKelamin"
            value={formData.jenisKelamin}
            onChange={handleChange}
            className="w-full px-4 py-2 border rounded-md focus:outline-none focus:border-blue-500"
            required
          >
            <option value="">Pilih Jenis Kelamin</option>
            <option value="Laki-laki">Laki-laki</option>
            <option value="Perempuan">Perempuan</option>
          </select>
        </div>
        <div className="col-span-3 md:col-span-1">
          <label htmlFor="umur" className="block text-gray-700">
            Umur:
          </label>
          <input
            type="number"
            id="umur"
            name="umur"
            value={formData.umur}
            onChange={handleChange}
            className="w-full px-4 py-2 border rounded-md focus:outline-none focus:border-blue-500"
            required
          />
        </div>
        <div className="col-span-3 md:col-span-2">
          <label htmlFor="noHp" className="block text-gray-700">
            Nomor HP:
          </label>
          <input
            type="tel"
            id="noHp"
            name="noHp"
            value={formData.noHp}
            onChange={handleChange}
            className="w-full px-4 py-2 border rounded-md focus:outline-none focus:border-blue-500"
            required
          />
        </div>
        <div className="col-span-3 md:col-span-2">
          <label htmlFor="alamat" className="block text-gray-700">
            Alamat:
          </label>
          <textarea
            id="alamat"
            name="alamat"
            value={formData.alamat}
            onChange={handleChange}
            className="w-full px-4 py-2 border rounded-md focus:outline-none focus:border-blue-500"
            required
          ></textarea>
        </div>
        <div className="col-span-3">
          <button
            type="submit"
            className="w-full bg-blue-500 text-black py-2 rounded-md hover:bg-blue-600 focus:outline-none focus:bg-blue-600"
          >
            Submit
          </button>
        </div>
      </form>
    </div>
  );
};

export default FormInputKonsul;
